const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Post = require("./models/Post");
const Comment = require("./models/Comment");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [abu, anon359, anon777] = await User.create({
    username: 'Abu',
    password: 'Abu',
    token: nanoid()
  }, {
    username: 'Anon359',
    password: 'Anon359',
    token: nanoid()
  }, {
    username: 'Anon777',
    password: 'Anon777',
    token: nanoid()
  });

  const [post1, post2, post3, post4] = await Post.create({
    title: "Добро пожаловать",
    description: "Добро пожаловать форум, ведите себя благоразумно",
    image: "fixtures/post1.jpeg",
    userID: abu,
    datetime: "2021-04-21T07:45:15.575Z",
  }, {
    title: "Сап двач, есть одна тян. ЕОТ тред стартует здесь.",
    description: "В общем, ЕОТ, и я лиственник. Очкую подойти и пояснить ей за всю хурму",
    image: "",
    userID: anon359,
    datetime: "2021-04-21T07:47:15.575Z",
  }, {
    title: "Приветики-пистолетики",
    description: "",
    image: "fixtures/post3.jpeg",
    userID: anon777,
    datetime: "2021-04-22T06:45:15.575Z",
  }, {
    title: "Обновление правил",
    description: "Новые правила вступают в силу завтра",
    image: "fixtures/post4.jpeg",
    userID: abu,
    datetime: "2021-04-22T08:35:15.575Z",
  });

  await Comment.create({
    comment: "test1",
    postID: post1,
    userID: anon359,
  }, {
    comment: "test2",
    postID: post2,
    userID: anon777,
  }, {
    comment: "test3",
    postID: post1,
    userID: anon777,
  }, {
    comment: "test4",
    postID: post3,
    userID: abu,
  }, {
    comment: "test5",
    postID: post3,
    userID: anon777,
  }, {
    comment: "test6",
    postID: post1,
    userID: abu,
  }, {
    comment: "test7",
    postID: post3,
    userID: anon359,
  }, {
    comment: "test8",
    postID: post2,
    userID: abu,
  }, {
    comment: "test9",
    postID: post4,
    userID: anon777,
  }, {
    comment: "test10",
    postID: post2,
    userID: anon777,
  }, {
    comment: "test11",
    postID: post4,
    userID: anon359,
  }, {
    comment: "test12",
    postID: post4,
    userID: abu,
  });

  await mongoose.connection.close();
};

run().catch(console.error);