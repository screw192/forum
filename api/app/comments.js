const express = require("express");
const Comment = require("../models/Comment");
const auth = require("../middleware/auth");

const router = express.Router();

router.post("/", auth, async (req, res) => {
  try {
    const commentData = {...req.body};

    commentData.userID = req.user._id;

    const comment = new Comment(commentData);
    await comment.save();

    res.send(comment);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;