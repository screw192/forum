const express = require('express');
const path = require('path');
const multer = require('multer');
const {nanoid} = require('nanoid');
const Post = require("../models/Post");
const Comment = require("../models/Comment");
const auth = require("../middleware/auth");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const posts = await Post.find()
        .populate("userID", "username")
        .sort("-datetime");

    res.send(posts);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const post = await Post.findOne({_id: req.params.id}).populate('userID', 'username')
    const comments = await Comment.find({postID: req.params.id}).populate('userID', 'username');

    const postDetailedData = {
      post,
      comments,
    };

    res.send(postDetailedData);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, upload.single('image'), async (req, res) => {
  try {
    const postData = req.body;

    if (req.file) {
      postData.image = 'uploads/' + req.file.filename;
    }

    postData.userID = req.user._id;
    postData.datetime = new Date();

    const post = new Post(postData);
    await post.save();

    res.send(post);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;