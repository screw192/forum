import React, {useState} from 'react';
import {Button, Menu, MenuItem} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/userActions";
import {Link} from "react-router-dom";

const UserMenu = ({user}) => {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
      <>
        <Button
            component={Link}
            to="/add_post"
            color="inherit"
        >
          Add post
        </Button>
        <Button
            onClick={handleClick}
            color="inherit"
        >
          Hi, {user.username}!
        </Button>
        <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
        >
          <MenuItem>Profile</MenuItem>
          <MenuItem>My Account</MenuItem>
          <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
        </Menu>
      </>
  );
};

export default UserMenu;