import React from 'react';
import {AppBar, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import AnonymousMenu from "./Menu/AnonymousMenu";
import UserMenu from "./Menu/UserMenu";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom: theme.spacing(4)
  },
  title: {
    flexGrow: 1,
    textDecoration: "none",
  },
}));

const AppToolbar = () => {
  const classes = useStyles();

  const user = useSelector(state => state.user.user);

  return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography color="inherit" component={Link} to="/" variant="h6" className={classes.title}>
              Forum
            </Typography>
            {user ? (
                <UserMenu user={user}/>
            ) : (
                <AnonymousMenu/>
            )}
            {/*<Button color="inherit" component={Link} to="/login">Login</Button>*/}
            {/*<Button color="inherit" component={Link} to="/register">Register</Button>*/}
          </Toolbar>
        </AppBar>
      </div>
  );
};

export default AppToolbar;