import React, {useState} from 'react';
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import {Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {postComment} from "../../store/actions/commentActions";
import {fetchDetailedPost} from "../../store/actions/detailedPostActions";

const AddComment = ({postID}) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.user);

  const [commentData, setCommentData] = useState({
    comment: "",
    postID: postID,
    userID: user._id,
  });

  const inputChangeHandler = event => {
    setCommentData(prevState => ({
      ...prevState,
      comment: event.target.value,
    }));
  };

  const submitHandler = async e => {
    e.preventDefault();

    await dispatch(postComment({...commentData}, user.token));
    dispatch(fetchDetailedPost(postID));
  };

  return (
      <Grid
          container
          component="form"
          spacing={4}
          alignItems="center"
          onSubmit={submitHandler}
      >
        <Grid item xs>
          <TextField
              size="small"
              variant="outlined"
              id="comment"
              label="Your comment"
              value={commentData.comment}
              onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item>
          <Button type="submit" variant="contained" color="primary">
            Send
          </Button>
        </Grid>
      </Grid>
  );
};

export default AddComment;