import React from 'react';
import {Link} from "react-router-dom";
import {Avatar, Grid, makeStyles, Typography} from "@material-ui/core";
import SubjectIcon from '@material-ui/icons/Subject';

import {apiURL} from "../../config";


const useStyles = makeStyles({
  imageBox: {
    height: "100px",
    width: "100px",
    backgroundColor: "#eee",
    border: "1px solid #bbb",
  },
  blue: {
    color: "#ffffff",
    backgroundColor: "steelblue",
    fontSize: "48px",
    borderRadius: "3px",
  },
});


const PostItem = ({postID, postImage, postDatetime, postUsername, postTitle}) => {
  const classes = useStyles();

  let imageBox;
  if (Boolean(postImage)) {
    imageBox = (
        <Avatar
            className={classes.imageBox}
            variant="rounded"
            src={apiURL + "/" + postImage}
        />
    );
  } else {
    imageBox = (
        <Avatar variant="rounded" className={classes.imageBox}>
          <SubjectIcon className={classes.blue} />
        </Avatar>
    );
  }

  return (
      <Grid item container spacing={2}>
        <Grid item>
          {imageBox}
        </Grid>
        <Grid item xs container direction="column">
          <Grid item xs>
            <Typography variant="body1">
              {postDatetime} by {postUsername}
            </Typography>
          </Grid>
          <Grid item xs>
            <Typography component={Link} to={"/posts/" + postID} variant="body1">
              {postTitle}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
  );
};

export default PostItem;