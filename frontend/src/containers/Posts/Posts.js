import React, {useEffect} from 'react';
import {Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";

import {fetchPosts} from "../../store/actions/postsActions";
import PostItem from "./PostItem";


const Posts = () => {
  const dispatch = useDispatch();
  const postsData = useSelector(state => state.posts.posts);

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);

  const postItems = postsData.map(item => {
    return (
      <PostItem
          key={item._id}
          postID={item._id}
          postImage={item.image}
          postDatetime={item.datetime}
          postUsername={item.userID.username}
          postTitle={item.title}
      />
    );
  });

  return (
      <Grid container direction="column" spacing={2}>
        {postItems}
      </Grid>
  );
};

export default Posts;