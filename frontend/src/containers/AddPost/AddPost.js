import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid, Typography} from "@material-ui/core";

import PostForm from "../../components/PostForm/PostForm";
import {createPost} from "../../store/actions/postsActions";
import {historyPush} from "../../store/actions/historyActions";


const AddPost = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.user);

  if (!user) {
    dispatch(historyPush("/"));
  }

  const onPostFormSubmit = async (postData, token) => {
    await dispatch(createPost(postData, token));
    await dispatch(historyPush("/"));
  };

  return (
      <Grid container direction="column" spacing={2}>
        <Grid item xs>
          <Typography variant="h4">New post</Typography>
        </Grid>
        <Grid item xs>
          <PostForm onSubmit={onPostFormSubmit} user={user}/>
        </Grid>
      </Grid>
  );
};

export default AddPost;