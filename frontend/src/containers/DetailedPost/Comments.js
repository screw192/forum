import React from 'react';
import {Typography} from "@material-ui/core";
import CommentItem from "./CommentItem";
import AddComment from "../AddComment/AddComment";
import {useSelector} from "react-redux";

const Comments = ({commentsData, postID}) => {
  const user = useSelector(state => state.user.user);

  const comments = commentsData.map(item => {
    return (
        <CommentItem
            key={item._id}
            username={item.userID.username}
            comment={item.comment}
        />
    );
  });

  return (
      <>
        <Typography variant="h6">Comments:</Typography>
        {comments}
        {user ? (<AddComment postID={postID}/>) : null}
      </>
  );
};

export default Comments;