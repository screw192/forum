import React from 'react';
import {Grid, Typography} from "@material-ui/core";

const CommentItem = ({username, comment}) => {
  return (
      <Grid item>
        <Typography variant="body2">{username}: {comment}</Typography>
      </Grid>
  );
};

export default CommentItem;