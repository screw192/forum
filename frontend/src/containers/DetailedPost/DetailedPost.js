import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchDetailedPost} from "../../store/actions/detailedPostActions";
import {Grid, LinearProgress, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import Comments from "./Comments";

const DetailedPost = props => {
  const dispatch = useDispatch();
  const postData = useSelector(state => state.detailedPost.post);
  const commentsData = useSelector(state => state.detailedPost.comments);
  const postLoading = useSelector(state => state.detailedPost.detailedPostLoading);

  const postID = props.match.params.id;

  useEffect(() => {
    dispatch(fetchDetailedPost(postID));
  }, [dispatch, postID]);

  let imageBox = null;
  if (Boolean(postData.image)) {
    imageBox = (
        <Grid item>
          <img src={apiURL + "/" + postData.image} alt="" height="200px" width="auto"/>
        </Grid>
    );
  }

  return (
      <Grid container spacing={4} direction="column">
        {postLoading ? (
            <LinearProgress />
        ) : (
            <>
              <Grid item container>
                {imageBox}
                <Grid item xs>
                  <Typography variant="h4" paragraph>{postData.userID.username}: {postData.title}</Typography>
                  <Typography variant="body1" paragraph>{postData.description}</Typography>
                </Grid>
              </Grid>
              <Grid item container direction="column" alignItems="center">
                <Comments commentsData={commentsData} postID={postData._id}/>
              </Grid>
            </>
        )}


      </Grid>
  );
};

export default DetailedPost;