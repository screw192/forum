import {
  FETCH_DETAILED_POST_FAILURE,
  FETCH_DETAILED_POST_REQUEST,
  FETCH_DETAILED_POST_SUCCESS
} from "../actions/detailedPostActions";

const initialState = {
  post: {},
  comments: [],
  detailedPostLoading: true,
  detailedPostError: null,
};

const detailedPostReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DETAILED_POST_REQUEST:
      return {...state, detailedPostLoading: true};
    case FETCH_DETAILED_POST_SUCCESS:
      return {...state, detailedPostLoading: false, post: action.detailedPostData.post, comments: action.detailedPostData.comments};
    case FETCH_DETAILED_POST_FAILURE:
      return {...state, detailedPostLoading: false, detailedPostError: action.error};
    default:
      return state;
  }
};

export default detailedPostReducer;