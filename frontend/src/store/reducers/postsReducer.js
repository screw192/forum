import {FETCH_POSTS_FAILURE, FETCH_POSTS_REQUEST, FETCH_POSTS_SUCCESS} from "../actions/postsActions";

const initialState = {
  posts: [],
  postsLoading: false,
  postsError: null,
};

const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS_REQUEST:
      return {...state, postsLoading: true};
    case FETCH_POSTS_SUCCESS:
      return {...state, postsLoading: false, posts: action.postsData};
    case FETCH_POSTS_FAILURE:
      return {...state, postsLoading: false, postsError: action.error};
    default:
      return state;
  }
};

export default postsReducer;