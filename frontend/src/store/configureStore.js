import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import postsReducer from "./reducers/postsReducer";
import detailedPostReducer from "./reducers/detailedPostReducer";
import userReducer from "./reducers/userReducer";

const rootReducer = combineReducers({
  posts: postsReducer,
  detailedPost: detailedPostReducer,
  user: userReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunkMiddleware))
);

store.subscribe(() => {
  saveToLocalStorage({
    user: store.getState().user
  });
});

export default store;