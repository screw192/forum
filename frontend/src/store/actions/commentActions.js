import axiosApi from "../../axiosApi";

export const POST_COMMENT_SUCCESS = "POST_COMMENT_SUCCESS";

export const postCommentSuccess = () => ({type: POST_COMMENT_SUCCESS});

export const postComment = (commentData, token) => {
  return async dispatch => {
    await axiosApi.post('/comments', commentData, {headers: {Authorization: token}});
    dispatch(postCommentSuccess());
  };
};