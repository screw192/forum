import axiosApi from "../../axiosApi";

export const FETCH_DETAILED_POST_REQUEST = "FETCH_DETAILED_POST_REQUEST";
export const FETCH_DETAILED_POST_SUCCESS = "FETCH_DETAILED_POST_SUCCESS";
export const FETCH_DETAILED_POST_FAILURE = "FETCH_DETAILED_POST_FAILURE";

export const fetchDetailedPostRequest = () => ({type: FETCH_DETAILED_POST_REQUEST});
export const fetchDetailedPostSuccess = detailedPostData => ({type: FETCH_DETAILED_POST_SUCCESS, detailedPostData});
export const fetchDetailedPostFailure = error => ({type: FETCH_DETAILED_POST_FAILURE, error});

export const fetchDetailedPost = postID => {
  return async dispatch => {
    try {
      dispatch(fetchDetailedPostRequest());

      const result = await axiosApi.get("/posts/" + postID);
      dispatch(fetchDetailedPostSuccess(result.data));
    } catch (e) {
      dispatch(fetchDetailedPostFailure(e));
    }
  };
};