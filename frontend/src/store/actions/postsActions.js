import axiosApi from "../../axiosApi";

export const FETCH_POSTS_REQUEST = "FETCH_POSTS_REQUEST";
export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS";
export const FETCH_POSTS_FAILURE = "FETCH_POSTS_FAILURE";

export const CREATE_POST_REQUEST = "CREATE_POST_REQUEST";
export const CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS";
export const CREATE_POST_FAILURE = "CREATE_POST_FAILURE";

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = postsData => ({type: FETCH_POSTS_SUCCESS, postsData});
export const fetchPostsFailure = error => ({type: FETCH_POSTS_FAILURE, error});

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createPostFailure = error => ({type: CREATE_POST_FAILURE, error});

export const fetchPosts = () => {
  return async dispatch => {
    try {
      dispatch(fetchPostsRequest());

      const response = await axiosApi.get("/posts");
      dispatch(fetchPostsSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostsFailure(e));
    }
  };
};

export const createPost = (postData, token) => {
  return async dispatch => {
    try {
      dispatch(createPostRequest());

      await axiosApi.post("/posts", postData, {headers: {Authorization: token}});
      dispatch(createPostSuccess());
    } catch (e) {
      dispatch(createPostFailure(e));
    }
  };
};