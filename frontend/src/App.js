import React from 'react';
import {Route, Switch} from 'react-router-dom';
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";

import Posts from "./containers/Posts/Posts";
import DetailedPost from "./containers/DetailedPost/DetailedPost";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import AddPost from "./containers/AddPost/AddPost";


const App = () => (
    <>
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="md">
          <Switch>
            <Route path="/" exact component={Posts} />
            <Route path="/posts/:id" exact component={DetailedPost} />
            <Route path="/login" exact component={Login} />
            <Route path="/register" exact component={Register} />
            <Route path="/add_post" exact component={AddPost} />
          </Switch>
        </Container>
      </main>
    </>
);

export default App;